import { Component, OnInit,Input } from '@angular/core';
import { BookService } from '../book.service';

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  name:string;
  author:string;
  read:boolean
  key:string;
  showTheField = false;
  showEditField = false;
  tempText='';
  showButton(){
    this.showTheField = true;
  }
  hideButton(){
    this.showTheField = false;
  }
  saveName(){
    this.bookService.updateName(this.key, this.name);
    this.showEditField = false;
  }
  saveAuthor(){
    this.bookService.updateAuthor(this.key, this.author);
    this.showEditField = false;
  }
  showEdit(){
    this.showTheField = false;
    this.showEditField = true;
    this.tempText = this.name;
 
  }
  cancel(){
    this.showEditField = false;
    this.name = this.tempText;
  }
 

  checkChange()
  {
   
    this.bookService.updateRead(this.key,this.read);
  }

  constructor(private bookService:BookService) { }

  ngOnInit() {
    
    this.name = this.data.name;
    this.author=this.data.author;
    this.read=this.data.read;
    this.key = this.data.$key;
  }

}
