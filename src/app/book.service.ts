import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  deleteItem(key:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').remove(key);
    })
  }

  updateRead(key:string,read:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').update(key,{'read':read});
    })
    
  }
  updateName(key:string, name:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').update(key,{'name':name});
    })
  }
  updateAuthor(key:string, author:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').update(key,{'author':author});
    })
  }
  addBook(name:string,author:string, read:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').push({'name':name,'author':author, 'read':false});
    })
  }
  

  user:Observable<firebase.User>;
  constructor(private db:AngularFireDatabase, 
              private authService:AuthService) { }
}
