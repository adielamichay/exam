import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';



@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: string;
  password: string;
  email:string;
  passwordCheck:string;
  nickname:string;
  code = '';
  message = '';
  required = '';
  require=true;
  hide=true;
  valid=false;
  validation='';


  tosign(){
    
    this.valid = false;
    this.require = true;

    if (this.name == null || this.password == null || this.email == null||this.passwordCheck== null||this.nickname== null) 
    {
      this.required = "this input is required";
      this.require = false;
    }

  if(this.require)
    {
      
        if (this.password==this.passwordCheck)
        {
          this.valid = true;
        }
      
    }
     if (this.valid && this.require)
    {
      this.authService.signUp(this.email,this.password)
      .then(value => { 
                      this.authService.updateProfile(value.user,this.name);
                      this.authService.addUser(value.user, this.name, this.email, this.nickname);
                      }).then(value =>{
                                      this.router.navigate(['/books']);
                                      }).catch(err => { 
                                                      this.code = err.code;
                                                      this.message = err.message;
                                                      console.log("error" + this.code);
                                                      console.log("output" + this.message);
                                                     })
    }
    else
    {
      this.validation = "Password must be identical";
    }
  }
 
  
  constructor(private router:Router,
              private authService:AuthService
              
  ) { }

  ngOnInit() {
   
  }

}
