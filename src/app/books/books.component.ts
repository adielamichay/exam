import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { BookService } from '../book.service';


@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  itemTextFromTodo='No books so far';
  books = [ ];
  user=[];
  nickname='';
  name:string;
  author:string;
  read:boolean
  key;
  addBook(){
    this.bookService.addBook(this.name, this.author, this.read);
    this.name = '';
    this.read=false;
    this.author='';
  }
  

  
  constructor(
    public authService:AuthService,
    private db:AngularFireDatabase,
  private bookService:BookService) { 

   
  }
   
  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid).snapshotChanges().subscribe(
        users => {
          users.forEach(
                nick => {
                  let y = nick.payload.toJSON();
                  this.user.push(y);            
                   this.nickname = this.user[0].nickname;      
            }
          )
          
         
        })
      })
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe(
        books =>{
          this.books = [];
          books.forEach(
            book => {
              let y =book.payload.toJSON();
              y["$key"] = book.key;
              this.books.push(y);
              console.log("user"+user);
              console.log(this.books);
              
            }
          )
        }
      ) 
    })
  }
}
