import { Component, OnInit } from '@angular/core';
import{AuthService} from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  password:string;
  
  code= '';
  message= '';
  required:string;
  require=true;
  hide=true;

  toLogin(){

    this.require=true;

    if ( this.password == null || this.email == null) 
    {
      this.required = "this input is required";
      this.require = false;
    }

  
    else if (this.require)
    {
      this.authService.login(this.email, this.password)
      .then(user => {
            this.router.navigate(['/welcome']);
           }).catch(err => {
              this.code = err.code;
              this.message = err.message;
              console.log("error" + this.code);
               console.log("output" + this.message);
            })
    }
   
  }

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

}
